﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="Our-Services.aspx.vb" Inherits="Our_Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section>
        <div class="rows inner_banner inner_banner_2">
            <div class="container">
                <p style="font-size:15px;font-weight:600;">Our Services – Through Online</p>
                <p>1. Domestic Flights</p>
                <p>2. International Flights</p>
                <p>3. Domestic Money Transfer</p>
                <p>4. Utilities – Pay your Bills and Mobile Recharge</p>
            </div>
        </div>
    </section>

    <br />


    <section class="tourb2-ab-p-2 com-colo-abou">
        <div class="container">

           <p style="font-size:15px;font-weight:600;">Our Services – Through Offline</p>
                <p>1. Hotels – Domestic and International</p>
                <p>2. Fix Departures –Domestic & International – Specially for Middle East and Canada</p>
                <p>3. Domestic Money Transfer</p>
                <p>4. Packages – Domestic and International</p>

        </div>




    </section>

</asp:Content>

