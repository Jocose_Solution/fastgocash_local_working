CREATE proc sp_SMBPInsertUpdatePayment
(
	@BBPSPaymentId varchar(100),
	@AgentId varchar(100),
	@ClientRefId varchar(100),
	@Number varchar(100),
	@SPKey varchar(50),
	@CircleID varchar(50),
	@Amount varchar(50),
	@TransactionId varchar(100),
	@AvlBalance varchar(100),	
	@OptTransactionId varchar(100),
	@Status varchar(max),
	@Type varchar(50),
	@Refund bit,
	@RefundId varchar(100),
	@Id int output
)
as
begin
	if(@Type='insert')		
			begin				
				insert into T_SMBP_BBPSPayment (AgentId,ClientRefId,Number,SPKey,CircleID,Amount)
				 values (@AgentId,@ClientRefId,@Number,@SPKey,@CircleID,@Amount)

				 SET @Id=@@IDENTITY
				 RETURN @Id
			end
	if(@Type='update')		
			begin
				update T_SMBP_BBPSPayment
				set TransactionId=@TransactionId,AvlBalance=@AvlBalance,OptTransactionId=@OptTransactionId,
				Status=@Status,Refund=@Refund,RefundId=@RefundId,UpdatedDate=getdate()
				where BBPSPaymentId=@BBPSPaymentId

				SET @Id=@@IDENTITY
				RETURN @Id
			end
end