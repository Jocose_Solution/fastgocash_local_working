﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="Term_Conditions.aspx.vb" Inherits="Term_Conditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="container">
    <h1>TERMS & CONDITIONS</h1>
<p>Obligation(s) of the User of this website</p>

<p>It is an obligation of the User of this Web Site to read the Terms and Conditions carefully and place an order for availing services of M/S GANDHI SERVICES only after he/she/it/they have very well understood the same. In the event of placing of order by the User of this Web Site and after making of the payment, it will be assumed by us and also by M/S GANDHI SERVICES that the User of this Web Site has understood the terms and conditions.  It is also clarified that the clauses of terms and conditions keep changing, without any notice, at the behest of M/S GANDHI SERVICES The changes are inevitable due to changes in the policy of Government, Airlines and other attending circumstances beyond the control of M/S GANDHI SERVICES It is therefore the duty and obligation of the User to go through the Terms and Conditions, every time he/she/it/they purchase the Travel Product.</p>

<p>It is an obligation of the User of this Web Site to provide correct and precise details/ particulars about himself/herself/itself/ themselves, i.e. just the same what has been given in the passport or other identifications Card/ document(s). Any problem, trouble, loss or damage due to furnishing of incorrect particulars/ details will be at the risk, consequences and peril of the User of this Web Site himself/herself/itself/ themselves.</p>   

<p style="font-weight:600;">Site and its Contents</p>

<p>This Site is only for your personal use. You shall not distribute, exchange, modify, sell or transmit anything you copy from this Site, including but not limited to any text, images, audio and video, for any business, commercial or public purpose.</p>

<p>As long as you comply with the terms of these Terms and Conditions of use, M/s Gandhi Enterprises Pvt Ltd grants you a non-exclusive, non-transferable, limited right to enter, view and use this site. You agree not to interrupt or attempt to interrupt the operation of this Site.</p>

<p>Access to certain areas of the Site may only be available to registered members. To become a registered member, you may be required to answer certain questions. Answers to such questions may be mandatory and/or optional. You represent and warrant that all information you supply to us, about yourself, and others, is true and accurate.</p>

<p style="font-weight:600;">Disclaimer</p>

<p>Our Web Site namely fastgocash.com and M/S GANDHI SERVICES have endeavoured to ensure that all the information on the Web Site is correct, however, neither our Web Site nor M/S GANDHI SERVICES will be responsible for any inaccuracy, in-completeness, error in the data and material lying on our Web Site. The User of this Web Site deals with us at his own risk, consequences and peril. The User of this Web Site  understands it well that our services are squarely dependent on others like Internet Service Providers, Host of our Server, , Airlines, Hoteliers and Tour operator etc. and the same are not fully in our control.</p>

<p style="font-weight:600;">Right to Access</p>

<p>Our Web Site namely fastgocash.com and M/S GANDHI SERVICES reserve their right, in their sole discretion, to stop the access of our Customer(s) /the User of this Web Site to our Web Site and that of the Gandhi Services at any hour.</p>


<p style="font-weight:600;">Indemnification</p>

<p>Our Customer(s)s / The User of this Web Site(s)s agree to indemnify, defend and hold harmless. fastgocash.com from and against any and all losses, liabilities, claims, damages, costs and expenses (including legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by fastgocash.com that arise out of, result from, or may be payable by virtue of, any breach or non-performance of any representation, warranty, covenant or agreement made or obligation to be performed by  our Customer(s)s/the User of this Web Site.</p>

<p style="font-weight:600;">Termination of agreement and services</p>
<p>Our Web Site namely fastgocash.com and M/S GANDHI SERVICES reserve their right, in their sole discretion, to terminate without any notice the agreement and services agreed upon between ourUser of this Web Site / the Customer and  M/S GANDHI SERVICES </p> 

<p style="font-weight:600;">Notices and Communications</p>

<p>All notices and communication shall be needed to be directed to and served without fail at M/S GANDHI SERVICES Office at 4 Zila Parishat Gate Opposite A D Basic Chaupla Road Bareilly.</p>

<p style="font-weight:600;">Dispute and Jurisdiction</p>

<p>Any dispute between the User/ Customer of this Web Site and M/S GANDHI SERVICES shall be subject to jurisdiction of Delhi/New Delhi Courts only. However, neither the User/ Customer of this Web Site nor M/S GANDHI SERVICES will move the court directly. Both, the User/ Customer of this Web Site and M/S GANDHI SERVICES shall need to get their dispute resolved through Arbitration and the Arbitrator shall be appointed by M/S GANDHI SERVICES  only. The Award of the Arbitrator will be final subject to the law of the land.</p>

<p style="font-weight:600;">Terms & conditions</p>

<p>It is well understood by the User of this Web Site that M/S GANDHI SERVICES shall not be liable for any act of commission and omission beyond its control. It is also clarified by M/S GANDHI SERVICES and well understood by the User of this Web Site that M/S GANDHI SERVICES has no control on the services on ground, as provided by Airlines, Hoteliers, Tour Operators, and any other Third Party. M/S GANDHI SERVICES engages /hires the services of such agencies/ third Party based on their reputation and past dealings with it. M/S GANDHI SERVICES can’t and don’t commit its Customer(s) more than what it has declared herein.</p>  

<p>M/S GANDHI SERVICES will not be responsible also for losses or damages, occasioned or caused due to act of commission or omission on the part of Airlines, Hoteliers, Tour Operators, any other Third Party.</p>

<p>M/S GANDHI SERVICES shall be having right to cancel or modify Travel Itineraries or Tickets Booking in the event of exigency. Any Travel Product agreed to be availed of/sold by M/S GANDHI SERVICES will be subject to availability of the same. The Terms & Conditions are enumerated here-in-under:-</p>

<p>1)   This Web Site namely, fastgocash.com, is owned and operated by M/S GANDHI SERVICES Office at 4 Zila Parishat Gate Opposite A D Basic Chaupla Road Bareilly. The Users/ Customers of this Web Site shall therefore be dealing with M/S GANDHI SERVICES for availing the services of any nature whatsoever. The order for the purchase of Travel Products (Air Tickets, Travel & Tours, Hotels, etc.) which the Users/ Customers of this Web Site place through this Web Site will be executed by M/S GANDHI SERVICES, as an Agent between the Users/ Customers and the Third Party Service Providers (Airlines, Transporters, Hoteliers, etc.) The payment shall also be made by the Users of this Web Site directly to M/S GANDHI SERVICES,Refund(s) of any nature what so ever will also be dealt with by M/S GANDHI SERVICES only.</p>

<p>2)   M/S GANDHI SERVICES provides one window service/solutions to our Customer(s) / the User of this Web Sites vis-à-vis Travel Products, such as Air tickets, Visa, Hotels, Travel & Tour and Other Ancillary Services which might be needed by the user of this Web Site for Tours & Travels.</p>

<p>3)   The job of M/S GANDHI SERVICES includes purchase of product and services from service providers, for and on behalf of our Users/ Customer(s). It is well understood by our Users/ Customer(s) that M/S GANDHI SERVICES is not and shall not be a service provider. M/S GANDHI SERVICES is and shall be just the agent/ middleman between the Service Providers and the Users/ Customer(s). In other words, M/S GANDHI SERVICES shall be in the shoe of the Users/ Customer(s) of this Web Site. M/S GANDHI SERVICES shall be as much a consumer of service of the Service Providers as the Users/Customers, himself/herself/itself/themselves.</p>

<p>4)   The lapse/default in services of the Service Providers, loss or damage to baggage, death or personal injury to the Users/ Customers, delays or any other untoward incident or exigency occurring at different stages of Travel & Tour shall not be the responsibility of the M/S GANDHI SERVICES The Users/ Customers are availing the services of the Third Party Service Providers through M/S GANDHI SERVICES at their own risk, consequences and peril.M/S GANDHI SERVICES is only an agent who facilitates and arranges the services for the Users/ Customer(s) of this Web Site, to be ultimately provided by  Hoteliers, Tour operators, Airlines and other Third Party Service Providers.</p>


<p>5)   M/S GANDHI SERVICES has no control on the actual services to be provided on the ground by the Third Party Service Providers.</p>

<p>6)   Our Users/ Customer(s) dealing with M/S GANDHI SERVICES, undertake to sue and lay their claims in the Courts/ before the Arbitrator against the Service Providers at their own cost and consequences. M/S GANDHI SERVICES shall not be dragged in such litigation(s).

<p>7)   The information being displayed on our Web Site concerning the Service Providers is squarely and wholly supplied by the Service Providers themselves. Our Web Site or M/S GANDHI SERVICES shall not be held responsible for any discrepancy in the services advertised by the Service Providers through our Web Sites and actual services rendered by them at the ground. It will be the responsibility of the Users/ Customers to also find out themselves about the Service Providers before proceeding with them through us.</p>

<p>8)   M/S GANDHI SERVICES is and will be fully authorized and competent to make additions and alterations in the tour Itinerary and other travels programs without notice to the Users/ Customers of this Web Site.</p>

<p>9)   The Service fees & Other Taxes, applicable to the Travel Product, like bookings made online or via call centre of M/S GANDHI SERVICES or our Call Centre shall be liable to and payable by Users/ Customers of this Web Site.</p>

<p>10)   The Fares and Prices of the Travel Product including Airlines Tickets, as quoted on our Web Site will be subject to availability at the time of booking. The fare and prices are also subject to applicable taxes and charges, which our Customer(s)/the Users of this Web Site can find out and confirm with M/S GANDHI SERVICES before making any booking/payment. Nothing could be done post confirmation of booking/payments. Rates quoted are appropriate to the particular product at the time of quoting and these rates may change prior to the travel date. All prices are subject to availability and can be withdrawn or varied without notice.</p>

<p>11)   Final Payment: Final payment must be paid immediately when requested prior to travel date. No vouchers will be issued until final payment is received in our office. Final payment conditions may vary on product to product, these are shown on the individual pages. We will advise you in writing of these conditions at the time of booking. Please note the final payment may vary from the original booking price or quote if the product is subject to exchange rate fluctuations or price rises by wholesalers or other suppliers.</p>

<p>12)  Card Fees: Please note that a card fee will be applied automatically to credit card payment amounts:</p>

<p>This fee covers a range of costs associated with processing bookings paid for by credit card including the merchant fees ofthe various credit card companies, payment processing costs, administration costs, the cost of maintaining IT systems usedfor payment security to minimise credit card fraud, credit card chargebacks and associated fees.Payments made by cheque or direct deposit do not attract any fees.</p>

<p>13)  ADM Policy: Any ADM arising due to the below mentioned reasons will have to be borne by the Travel Partner</p>

<p>a)   GDS Misuse</p>
<p>b)   Fictitious Names or Fake Name Like Test Names / Wrong Names</p>
<p>c)   Duplicate Bookings</p>
<p>d)   Churning for same segment / Flight / Date</p>
<p>e)   Hold Bookings must be released or issued before Time Limit to avoid Agent Debit Memo (ADM)</p>
<p>f)    In Case the Booking is aborted at the time of Hold or Booking - Please do not issue tickets multiple times</p>

<p>Please contact our call centre for further action - ADM's will be borne by the agent if not guided by this policy</p>

<p>14)  Payment Processing Terms & Conditions:  By providing your credit card details and accepting our Terms & Conditions, you authorise M/s Gandhi Enterprises Pvt Ltd. to arrange for funds to be debited from your nominated credit card, in accordance with the terms & conditions of the Direct Debit Request Service Agreement as amended from time to time.</p>
 
<p>Your bank or credit card provider may apply currency conversion fees. Credit Cards are required to secure bookings if you are travelling within 14 days.</p>

<p>15)  Standard Cancellation Policy:Cancellation within 12 Hours of Scheduled Departure Time must be cancelled with the Airline directly, M/s Gandhi Services. will not be liable in any way if the passenger is No Show.</p>
 
<p>All Bookings cancelled may attract charges levied by .Cancellations must be in the form of Amendment on the portal.If a credit has been approved it is valid for 6 months from the date the cancellation was made.</p>

<p>16)  Flight Cancellation Policy: Flights booked on this website are governed by the terms and conditions of the airfare you purchased and are determined by the Airline (not M/s Gandhi Enterprises Pvt Ltd.).</p>

<p>a)   In most cases, airfares are fully non-refundable and non-transferable.</p>
<p>b)    Airline charges or part or full cancellation fees may apply to your particular airfare.</p>
<p>c)    A Travel Consultant will help you wherever possible within these terms and conditions.</p>
 
<p>17)  Special Cancellation Conditions:  Certain air, car, hotel and tour products will apply additional cancellation charges. These cancellation conditions and costs are located under the pricing on the individual pages and will be clearly advised to you in writing at time of booking.</p>

<p>18)  Amendment Fees: Any amendments made to confirmed bookings will incur a fee; The fees are charged per amendment.This is in addition to any fees that may be levied by the supplier or airline.</p>

<p>19)  Credit Card Chargeback Fees: Any fees charged to M/s Gandhi Enterprises Pvt Ltd by our credit card payment provider arising from a chargeback or a disputed charge on the cardholder's credit card will be charged to the cardholder. This fee is non-refundable.</p>

<p>20)  Change Of Itinerary After booking Has Commenced:  Any alteration or cancellation of services after your booking has commenced can incur penalties. There is no refund for unused services.</p>

<p>21)  Refunds: All refund requests must be in writing, and made direct to us or through the Agent from whom the travel arrangements were purchased.Claims must be made prior to departure time.Refunds will not be made for bookings cancelled due to inclement weather or illness. These must be claimed through your travel Insurance. No refunds will be made for services once travel arrangements have commenced.No guarantee is provided or warranted that any refund will be available. if payment done twice for one transaction, the one transaction amount will be refunded as Deposit in M/s Gandhi Services. Account within 07 to 10 working days</p>

<p>22)  Reporting of Incidents: Any abnormal incidents including injuries, service problems, cancellation of a service or dissatisfaction must be reported to M/s Gandhi Services. during the event to allow us an opportunity to rectify the situation or provide assistance.</p>

<p>23)  M/S GANDHI SERVICES has no control on the timing and quality of services provided by Airlines such as, Airfares, Flight timings, Quality of Meals, served in Aircraft etc.  The information displayed on our Web Sites is also subject to change without any notice to the Users/ Customers of this Web Site.</p>

<p>24)  It is well understood by the Users of this Web Site /Customers that they will need to stay in constant touch with M/S GANDHI SERVICES, concerned Airlines, other Service Providers for availing services in time and in fair quality and quantity. Any lap(es) in following this directive by our Users Customer(s) shall be at the cost and consequences of our Users Customer(s) only.</p>

<p>25)  The Passport and Visas are a big issue while travelling abroad. It will be the responsibility of the Users of this Web Site /Customers to keep them intact while travelling abroad. The validity of Visas and Passports, procurement of other documents like immigration checking etc. will be looked into, procured and ensured intact by our Users /Customer(s)s only. M/S GANDHI SERVICES shall not be anywhere in the picture unless specifically requested for the same and M/S GANDHI SERVICES also acceded to such request in writing.</p>

<p>26)  The Discounts are offered by M/S GANDHI SERVICES from time to time. However the percentage of such discount(s) is never fixed. It keeps floating. Our Customer(s)/User(s) will need to have a separate written arrangement for discounts with M/S GANDHI SERVICES In the absence of such written arrangement, Complaint of any sorts concerning discrepancy(ies) in the discounts of any nature what so ever shall not be entertained by us or by M/S GANDHI SERVICES</p>

<p>27)  Our Web Site has taken all due care and steps to ensure the accuracy of the information lying on this Web Site. However, neither our Web Site nor M/S GANDHI SERVICES shall be responsible for any error(s), inaccuracy(ies), omissions(s) creping in the material due to human error or otherwise. We further declare that our Web Site or M/S GANDHI SERVICES shall not be responsible also for transmission errors, technical defects, interruptions, third party intervention or viruses, etc.M/S GANDHI SERVICES or our Web Site don’t and can’t guarantee the uninterrupted availability of this Web Site and therefore consequential loses, costs and/ or damages while dealing with us </p>

<p>28)  M/S GANDHI SERVICES through us will be borne by our Users/ Customers only and not by us/Web Site or M/S GANDHI SERVICES</p>

<p>29)  Our Web Site protects the online privacy of our Users / Customer(s) but our Users/ Customers agree that we can use the information / details provided by them to us for enhancing the prospectus of the business of our Web Site or that of M/S GANDHI SERVICES</p>

<p>30)  Our Customer(s) / Users opting to take services of M/S GANDHI SERVICES by making payment through our Web Site shall be liable to pay applicable charges, fees, duties, taxes, levies and assessments, etc.</p>

<p>31)  The Users/ Customers of this Web Site shall be dealing with and making the payment to M/S GANDHI SERVICES subject to aforementioned terms and conditions.</p>

    </div>
</asp:Content>

