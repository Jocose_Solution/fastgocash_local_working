﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using InstantPayServiceLib;
using Newtonsoft.Json.Linq;

public partial class DMT_Manager_dmt_SenderDetails : System.Web.UI.Page
{
    private static string UserId { get; set; }
    private static string Mobile { get; set; }
    private static string RemitterId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();

            Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
            RemitterId = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId))
            {
                Response.Redirect("/");
            }
        }
        else
        {
            Response.Redirect("/");
        }
    }

    #region [Json Section]
    public static List<string> GetRemitterDetail()
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> BindRemitterDetails()
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                DataTable dtRemitter = InstantPay_ApiService.GetT_InstantPayRemitterRegResponse(UserId, RemitterId, Mobile);
                if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                {
                    string sendername = dtRemitter.Rows[0]["Name"].ToString();
                    string mobile = dtRemitter.Rows[0]["Mobile"].ToString();
                    string address = dtRemitter.Rows[0]["Address"].ToString() + ", " + dtRemitter.Rows[0]["State"].ToString() + ", " + dtRemitter.Rows[0]["PinCode"].ToString();
                    string creditLimit = dtRemitter.Rows[0]["CreditLimit"].ToString();
                    string remainingLimit = dtRemitter.Rows[0]["RemainingLimit"].ToString();
                    string consumedAmount = dtRemitter.Rows[0]["ConsumedAmount"].ToString();
                    string localAddress = !string.IsNullOrEmpty(dtRemitter.Rows[0]["LocalAddress"].ToString()) ? dtRemitter.Rows[0]["LocalAddress"].ToString() : "- - -";
                    result.Add(SenderHTMLDetails(sendername, mobile, address, localAddress, creditLimit, remainingLimit, consumedAmount));
                }

                result.Add(SenderBenHTMLDetails(RemitterId));
            }
        }
        else
        {
            result.Add("reload");
        }

        return result;
    }

    [WebMethod]
    public static List<string> BeneficiaryRegistration(string benmobile, string accountno, string bankname, string ifsccode, string name)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string response = InstantPay_ApiService.BeneficiaryRegistration(RemitterId, name, benmobile, accountno, bankname, ifsccode, UserId);
                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            if (ReBindRemitterApiDetail())
                            {
                                result.Add("success");
                                result.Add(SenderBenHTMLDetails(RemitterId));
                            }
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add(statusMessage);
                        }
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> FundTransfer(string benid, string amount)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                    DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                    {
                        agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                        agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                        if (Convert.ToDouble(agencyCreditLimit) > 0)
                        {
                            DataTable dtBenDetail = InstantPay_ApiService.GetBenDetailsByRemitterId(RemitterId, benid);
                            if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                            {
                                string transferMode = dtBenDetail.Rows[0]["imps"].ToString() == "1" ? "IMPS" : "NEFT";

                                string trackid = "DMT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

                                SqlTransactionDom objDom = new SqlTransactionDom();
                                int isLedger = objDom.Ledgerandcreditlimit_Transaction(UserId, Convert.ToDouble(amount), trackid, "", "", agencyName, GetLocalIPAddress(), "", "", "", Convert.ToDouble(agencyCreditLimit.Trim()), "DMT_Fund_Transfer");

                                if (isLedger > 0)
                                {
                                    string response = InstantPay_ApiService.FundTransfer(Mobile, benid, amount, transferMode, UserId, trackid, RemitterId);

                                    if (!string.IsNullOrEmpty(response))
                                    {
                                        dynamic dyResult = JObject.Parse(response);
                                        string statusCode = dyResult.statuscode;
                                        string statusMessage = dyResult.status;

                                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                                        {
                                            result.Add("success");
                                        }
                                        else if (statusCode.ToLower() == "tup" && statusMessage.ToLower() == "transaction under process")
                                        {
                                            result.Add("under_process");
                                        }
                                        else if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "transaction failed")
                                        {
                                            result.Add("error");
                                        }
                                        else if (statusCode.ToLower() == "dtx" && statusMessage.ToLower() == "duplicate transaction")
                                        {
                                            result.Add("duplicate");
                                        }

                                        result.Add(statusMessage + "<br/> Track ID : " + trackid);
                                    }
                                }
                                else
                                {
                                    result.Add("failed");
                                    result.Add("We are unable to transfer money at the moment. Instead of trying again, please contact our call centre to avoid any inconvenience.");
                                }
                            }
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add("Insufficient credit limit !");
                        }
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add("Agency does not exist!");
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> GetTransactionHistory()
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    DataTable dtTrans = InstantPay_ApiService.GetTransactionHistory(Mobile, RemitterId);

                    result.Add("success");
                    result.Add(BindTransDetails(dtTrans));
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string BindTransDetails(DataTable dtTrans)
    {
        StringBuilder sbTrans = new StringBuilder();

        if (dtTrans != null && dtTrans.Rows.Count > 0)
        {
            for (int i = 0; i < dtTrans.Rows.Count; i++)
            {
                string status = dtTrans.Rows[i]["Status"].ToString();
                if (status.ToLower().Trim() == "transaction under process")
                {
                    status = "<td class='text-warning'>Under Process</td>";
                }
                else if (status.ToLower().Trim() == "transaction successful")
                {
                    status = "<td class='text-success'>Success</td>";
                }
                else if (status.ToLower().Trim() == "duplicate transaction")
                {
                    status = "<td class='text-danger'>Duplicate</td>";
                }
                else if (status.ToLower().Trim() == "transaction failed")
                {
                    status = "<td class='text-danger'>Failed</td>";
                }
                else
                {
                    status = "<td>--</td>";
                }

                sbTrans.Append("<tr>");
                sbTrans.Append("<td>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["UpdatedDate"].ToString()) + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["ipay_id"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["orderid"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["ref_no"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TrackId"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TxnMode"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["Amount"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["charged_amt"].ToString() + "</td>");
                sbTrans.Append("<td><span class='text-primary' title='View' style='cursor: pointer;' onclick='ShowBenDetails(" + dtTrans.Rows[i]["BenificieryId"].ToString() + ")'>" + dtTrans.Rows[i]["BenName"].ToString() + "</span></td>");
                sbTrans.Append(status);
                //sbTrans.Append("<td>--</td>");
                string url = "/dmt-manager/printtrans.aspx?mobile=" + Mobile + "&sender=" + RemitterId + "&beneficaryid=" + dtTrans.Rows[i]["BenificieryId"].ToString() + "&trackid=" + dtTrans.Rows[i]["TrackId"].ToString() + "";

                sbTrans.Append("<td><a href='" + url + "' target='_blank' class='text-primary'>Print</a></td>");
                sbTrans.Append("</tr>");
            }
        }
        else
        {
            sbTrans.Append("<tr>");
            sbTrans.Append("<td colspan='12' class='text-danger text-center'>Record not found !</td>");
            sbTrans.Append("</tr>");
        }

        return sbTrans.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static string GetBeneficiaryDetailById(string benid)
    {
        StringBuilder result = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                DataTable dtBenDetail = InstantPay_ApiService.GetBenDetailsByRemitterId(RemitterId, benid);
                if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
                {
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Beneficary ID</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["BeneficiaryId"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Name</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["Name"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Account</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["Account"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>IFSC Code</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["IfscCode"].ToString() + "</div></div>");
                    result.Append("<div class='row' style='padding-bottom: 5px;'><div class='col-sm-4'>Bank</div><div class='col-sm-8'>" + dtBenDetail.Rows[0]["Bank"].ToString() + "</div></div>");
                }
            }
        }

        return result.ToString();
    }

    [WebMethod]
    public static List<string> GetFilterTransactionHistory(string fromdate, string todate, string trackid)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                DataTable dtTrans = InstantPay_ApiService.GetFilterTransactionHistory(RemitterId, fromdate, todate, trackid);

                result.Add("success");
                result.Add(BindTransDetails(dtTrans));
            }
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficialRecord(string benid)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string response = InstantPay_ApiService.BeneficiaryRemove(benid, RemitterId, UserId);

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                        {
                            result.Add("success");
                        }
                        else if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            result.Add("success");
                        }
                        else
                        {
                            result.Add("failed");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficiaryVarification(string benid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string response = InstantPay_ApiService.BeneficiaryRemoveValidate(benid, RemitterId, otp, UserId);

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            result.Add("success");
                            result.Add(SenderBenHTMLDetails(RemitterId));
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add(statusMessage);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }
    #endregion

    private static bool ReBindRemitterApiDetail()
    {
        bool isSucces = false;

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                string result = InstantPay_ApiService.GetReSendRemitterDetail(Mobile, UserId);

                if (!string.IsNullOrEmpty(result))
                {
                    dynamic dyResult = JObject.Parse(result);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;
                        string remitterId = remitter.id;

                        dynamic beneficiary = dyData.beneficiary;

                        if (beneficiary != null)
                        {
                            foreach (var item in beneficiary)
                            {
                                string bank = item.bank;
                                string imps = item.imps;
                                string lastSuccessDate = item.last_success_date;
                                string lastSuccessImps = item.last_success_imps;
                                string lastSuccessName = item.last_success_name;

                                bool isUpdated = InstantPay_DataBase.Update_T_InstantPayBeneficary(remitterId, bank, imps, lastSuccessDate, lastSuccessImps, lastSuccessName);
                                if (!isUpdated)
                                {
                                    isSucces = false;
                                    break;
                                }
                                else
                                {
                                    isSucces = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return isSucces;
    }

    #region [Html Section Bind]
    public static string SenderHTMLDetails(string sendername, string mobile, string address, string localadd, string creditLimit, string remainingLimit, string consumedAmount)
    {
        StringBuilder sbSender = new StringBuilder();

        sbSender.Append("<div class='row boxSender'>");
        sbSender.Append("<div class='col-sm-6'><p class='p_botm'>Sender Details</p></div>");
        sbSender.Append("<div class='col-sm-6' style='padding: 2px;'><div class='row'><div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " (" + mobile + ")</p></div><div class='col-sm-6 text-right'><p class='p_botm btn btn-sm' style='border: 1px solid #fff;color: #fff;cursor: pointer;' id='UpdateLocalAddress'><i class='fa fa-edit'></i> Edit local Address</p></div></div></div>");
        //sbSender.Append("<div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " (" + mobile + ")</p></div>");
        sbSender.Append("</div>");

        sbSender.Append("<div class='row boxSenderdetail'>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>Name</p><p class='pmargin'>Address</p><p class='pmargin'>Current / Local Address</p><p class='pmargin'>Ledger Detail</p></div>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>" + sendername.ToUpper() + " (" + mobile + ")</p><p class='pmargin'>" + address + "</p><p class='pmargin'>" + localadd + "</p> <p class='pmargin'>Credit Limit: " + creditLimit + "/- ConsumedAmount: " + consumedAmount + "/- RemainingLimit: " + remainingLimit + "/-</p></div>");
        sbSender.Append("</div>");

        return sbSender.ToString();
    }

    public static string SenderBenHTMLDetails(string remitterId)
    {
        StringBuilder sbSender = new StringBuilder();

        DataTable dtBenDetail = InstantPay_ApiService.GetBenDetailsByRemitterId(remitterId);

        if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtBenDetail.Rows.Count; i++)
            {
                string benid = dtBenDetail.Rows[i]["BeneficiaryId"].ToString();

                sbSender.Append("<tr>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["Name"].ToString() + "</td>");
                sbSender.Append("<td>Other</td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["Bank"].ToString() + "</td>");
                if (dtBenDetail.Rows[i]["Status"].ToString() == "1")
                {
                    sbSender.Append("<td><span class='fa fa-check-circle accountvalid' data-toogle='tooltip' title='Account number is valid'></span></td>");
                }
                else
                {
                    sbSender.Append("<td><span class='fa fa-question-circle text-warning accountvalid' data-toogle='tooltip' title='A/C validation is pending'></span></td>");
                }
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["Account"].ToString() + "</td>");
                sbSender.Append("<td>" + dtBenDetail.Rows[i]["IfscCode"].ToString() + "</td>");
                sbSender.Append("<td><div class='form-validation'> <input type='text' class='form-control textboxamount' id='txtTranfAmount_" + benid + "' name='txtTranfAmount_" + benid + "' /></div></td>");
                sbSender.Append("<td><span class='btn btn-success btn-sm bentransfermoney' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnMoneyTransfer_" + benid + "' data-benid='" + benid + "'  onclick='DirectTransferMoney(" + benid + ");'>Transfer</span></td>");
                sbSender.Append("<td><span class='btn btn-danger btn-sm bendeleterecord' style='padding: 5px; font-size: 12px; font-weight: 500; color: #fff!important' id='btnDeleteBeneficiary_" + benid + "' data-benid='" + benid + "' onclick='DeleteBeneficialRecord(" + benid + ");'>Delete</span></td>");
                sbSender.Append("</tr>");
            }
        }
        else
        {
            sbSender.Append("<tr>");
            sbSender.Append("<td colspan='9' class='text-center text-danger'>Record not found !</td>"); ;
            sbSender.Append("</tr>");
        }

        return sbSender.ToString();
    }
    #endregion

    public static string GetLocalIPAddress()
    {
        string ipAddress = string.Empty;

        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                ipAddress = ip.ToString();
            }
        }

        return ipAddress;
    }

    [WebMethod]
    public static string BindAllBank()
    {
        StringBuilder result = new StringBuilder();

        try
        {
            DataTable dtBank = InstantPay_ApiService.GetBindAllBank();
            if (dtBank != null && dtBank.Rows.Count > 0)
            {
                result.Append("<option value=''>Select Bank</option>");
                for (int i = 0; i < dtBank.Rows.Count; i++)
                {
                    result.Append("<option value='" + dtBank.Rows[i]["branch_ifsc"].ToString() + "'>" + dtBank.Rows[i]["bank_name"].ToString() + "</option>");
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result.ToString();
    }

    [WebMethod]
    public static List<string> UpdateLocalAddress(string updatedaddress)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(updatedaddress.Trim()))
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    if (InstantPay_ApiService.UpdateLocalAddress(RemitterId, UserId, Mobile, updatedaddress))
                    {
                        result.Add("success");
                        result.Add("Address has been updated successfully.");
                    }
                }
            }
        }
        else
        {
            result.Add("empty");
            result.Add("Please enter current / local address !");
        }

        return result;
    }
}