﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="DMT_Registration.aspx.cs" Inherits="DMT_Manager_DMT_Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">
        #success_tic .page-body{
  max-width:300px;
  background-color:#FFFFFF;
  margin:10% auto;
}
 #success_tic .page-body .head{
  text-align:center;
}
/* #success_tic .tic{
  font-size:186px;
} */
#success_tic .close{
      opacity: 1;
    position: absolute;
    right: 0px;
    font-size: 30px;
    padding: 3px 15px;
  margin-bottom: 10px;
}
#success_tic .checkmark-circle {
  width: 150px;
  height: 150px;
  position: relative;
  display: inline-block;
  vertical-align: top;
}
.checkmark-circle .background {
  width: 150px;
  height: 150px;
  border-radius: 50%;
  background: #1ab394;
  position: absolute;
}
#success_tic .checkmark-circle .checkmark {
  border-radius: 5px;
}
#success_tic .checkmark-circle .checkmark.draw:after {
  -webkit-animation-delay: 300ms;
  -moz-animation-delay: 300ms;
  animation-delay: 300ms;
  -webkit-animation-duration: 1s;
  -moz-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-timing-function: ease;
  -moz-animation-timing-function: ease;
  animation-timing-function: ease;
  -webkit-animation-name: checkmark;
  -moz-animation-name: checkmark;
  animation-name: checkmark;
  -webkit-transform: scaleX(-1) rotate(135deg);
  -moz-transform: scaleX(-1) rotate(135deg);
  -ms-transform: scaleX(-1) rotate(135deg);
  -o-transform: scaleX(-1) rotate(135deg);
  transform: scaleX(-1) rotate(135deg);
  -webkit-animation-fill-mode: forwards;
  -moz-animation-fill-mode: forwards;
  animation-fill-mode: forwards;
}
#success_tic .checkmark-circle .checkmark:after {
  opacity: 1;
  height: 75px;
  width: 37.5px;
  -webkit-transform-origin: left top;
  -moz-transform-origin: left top;
  -ms-transform-origin: left top;
  -o-transform-origin: left top;
  transform-origin: left top;
  border-right: 15px solid #fff;
  border-top: 15px solid #fff;
  border-radius: 2.5px !important;
  content: '';
  left: 35px;
  top: 80px;
  position: absolute;
}

@-webkit-keyframes checkmark {
  0% {
    height: 0;
    width: 0;
    opacity: 1;
  }
  20% {
    height: 0;
    width: 37.5px;
    opacity: 1;
  }
  40% {
    height: 75px;
    width: 37.5px;
    opacity: 1;
  }
  100% {
    height: 75px;
    width: 37.5px;
    opacity: 1;
  }
}
@-moz-keyframes checkmark {
  0% {
    height: 0;
    width: 0;
    opacity: 1;
  }
  20% {
    height: 0;
    width: 37.5px;
    opacity: 1;
  }
  40% {
    height: 75px;
    width: 37.5px;
    opacity: 1;
  }
  100% {
    height: 75px;
    width: 37.5px;
    opacity: 1;
  }
}
@keyframes checkmark {
  0% {
    height: 0;
    width: 0;
    opacity: 1;
  }
  20% {
    height: 0;
    width: 37.5px;
    opacity: 1;
  }
  40% {
    height: 75px;
    width: 37.5px;
    opacity: 1;
  }
  100% {
    height: 75px;
    width: 37.5px;
    opacity: 1;
  }
}
    </style>



    <div class="container">
        <div class="login-signup-page mx-auto my-5">
            <h3 class="font-weight-400 text-center">Sign Up for DMT</h3>
            <p class="lead text-center">Your Sign Up information is safe with us.</p>
            <div class="bg-light shadow-md rounded p-4 mx-2">
                <form id="signupForm" runat="server">
                    <div class="form-group">
                        <label for="fullName">Mobile No.</label>
                        <input type="text" class="form-control" runat="server"  placeholder="Enter Mobile No." />
                    </div>
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <asp:Textbox type="email" class="form-control" runat="server"  placeholder="Enter First Name" />
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Surname</label>
                        <asp:Textbox type="password" class="form-control" runat="server"   placeholder="Enter Surname" />
                    </div>

                    <div class="form-group">
                        <label for="loginPassword">PIN Code</label>
                        <asp:Textbox type="password" class="form-control" runat="server"  placeholder="Enter PIN" />
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Outlet ID</label>
                        <asp:Textbox type="password"  class="form-control" runat="server"  placeholder="Enter Outlet ID" />
                    </div>

                    <%--<button class="btn btn-primary btn-block my-4" type="submit" onclick="">Sign Up</button>--%>
                    <button type="button" runat="server" data-toggle="modal" data-target="#success_tic" ID="btn_Registration" class="btn btn-primary btn-block my-4">Register</button>
                </form>
                <p class="text-3 text-muted text-center mb-0">Already have a DMT account? <a class="btn-link" href="login-3.html">Send Money</a></p>
            </div>
        </div>
        <!-- Content end -->


    </div>
    <div id="success_tic" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <a class="close" href="#" data-dismiss="modal">&times;</a>
      <div class="page-body">
    <div class="head">  
      <h3 style="margin-top:5px;">Registration Success</h3>
      <%--<h4>Lorem ipsum dolor sit amet</h4>--%>
    </div>

  <h1 style="text-align:center;"><div class="checkmark-circle">
  <div class="background"></div>
  <div class="checkmark draw"></div>

     

</div></h1>


          <div class="row">
      <a href="Send_Money.aspx" class="btn btn-primary btn-block my-4">OK</a>
          </div>
  </div>
</div>
       
    </div>

  </div>


</asp:Content>

