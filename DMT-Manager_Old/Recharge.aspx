﻿<%@ Page Title="" Language="VB" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="false" CodeFile="Recharge.aspx.vb" Inherits="DMT_Manager_Recharge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        #main-wrapper {
            background: #004c60 !important;
        }
    </style>
    <br />
    <br />
   
    <div class="col-md-12">
    
        <ul class="nav nav-pills" id="pillsmyTab" role="tablist">
            <li class="nav-item"><a class="nav-link active" id="firstTab-Pills" data-toggle="tab" href="#firstTabPills" role="tab" aria-controls="firstTabPills" aria-selected="true" style="color:#fff !important;">Mobile Recharge</a> </li>
            <li class="nav-item"><a class="nav-link" id="secondTab-Pills" data-toggle="tab" href="#secondTabPills" role="tab" aria-controls="secondTabPills" aria-selected="false" style="color:#fff !important;">DTH</a> </li>
            <li class="nav-item"><a class="nav-link" id="thirdTab-Pills" data-toggle="tab" href="#thirdTabPills" role="tab" aria-controls="thirdTabPills" aria-selected="false" style="color:#fff !important;">Electricity</a> </li>
            <li class="nav-item"><a class="nav-link" id="fourthTab-Pills" data-toggle="tab" href="#fourthTabPills" role="tab" aria-controls="fourthTabPills" aria-selected="false" style="color:#fff !important;">Landline</a> </li>
            <li class="nav-item"><a class="nav-link" id="fifthTab-Pills" data-toggle="tab" href="#fifthTabPills" role="tab" aria-controls="fifthTabPills" aria-selected="false" style="color:#fff !important;">Insurance</a> </li>


        </ul>

        <br />
        <br />
        <div class="tab-content bg-light shadow-sm rounded py-4 mb-4" id="pillsmyTabContent" style="padding: 10px;">
            <div class="tab-pane fade active show" id="firstTabPills" role="tabpanel" aria-labelledby="firstTab-Pills">
                <h3>Mobile Recharge</h3>
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Mobile Number" />
                        </div>
                         <div class="col-md-2">
                            <select id="withdrawto" class="custom-select" required="">
                  <option value="">--Select Operator--</option>
                  <option>Airtel</option>
                  <option>Vodafone</option>
                  <option>Jio</option>
                </select>
                        </div>
                         <div class="col-md-2">
                               <select id="Select1" class="custom-select" required="">
                  <option value="">--Change Circle--</option>
                  <option>Delhi</option>
                  <option>Haraiyana</option>
                  <option>Bihar</option>
                                    <option>Jharkhand</option>
                  <option>Madhya Pradesh</option>
                  <option>Tmil Nadu</option>
                </select>
                        </div>
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Amount" />
                        </div>


                        <div class="col-md-2">
                            <button class="btn btn-primary btn-block">Recharge</button>
                        </div>
                    </div>
                
            </div>
            <div class="tab-pane fade" id="secondTabPills" role="tabpanel" aria-labelledby="secondTab-Pills">
                <h3>DTH Recharge</h3>
                <div class="row">
                        <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Mobile Number" />
                        </div>
                         <div class="col-md-2">
                            <select id="Select2" class="custom-select" required="">
                  <option value="">--Select Operator--</option>
                  <option>Airtel</option>
                  <option>Vodafone</option>
                  <option>Jio</option>
                </select>
                        </div>
                   
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Enter Amount" />
                        </div>


                        <div class="col-md-2">
                            <button class="btn btn-primary btn-block">Recharge</button>
                        </div>
                    </div>
            </div>
            <div class="tab-pane fade" id="thirdTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">

                <h3>Pay your Electricity bill</h3>
               <div class="row">
                        <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Mobile Number" />
                        </div>
                         <div class="col-md-2">
                            <select id="Select3" class="custom-select" required="">
                  <option value="">--Select Operator--</option>
                  <option>Airtel</option>
                  <option>Vodafone</option>
                  <option>Jio</option>
                </select>
                        </div>
                   
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Enter Account No." />
                        </div>

                   
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Enter Amount" />
                        </div>

                        <div class="col-md-2">
                            <button class="btn btn-primary btn-block">Recharge</button>
                        </div>
                    </div>
            </div>

            <div class="tab-pane fade" id="fourthTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">

                <h3>Pay your Landline bill</h3>
               <div class="row">
                        <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Mobile Number" />
                        </div>
                         <div class="col-md-2">
                            <select id="Select4" class="custom-select" required="">
                  <option value="">--Select Operator--</option>
                  <option>Airtel</option>
                  <option>Vodafone</option>
                  <option>Jio</option>
                </select>
                        </div>
                   
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Enter Account No." />
                        </div>

                   
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Enter Amount" />
                        </div>

                        <div class="col-md-2">
                            <button class="btn btn-primary btn-block">Recharge</button>
                        </div>
                    </div>
            </div>

            <div class="tab-pane fade" id="fifthTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">

                <h3>Pay your Insurance bill</h3>
               <div class="row">
                        <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Mobile Number" />
                        </div>
                         <div class="col-md-2">
                            <select id="Select5" class="custom-select" required="">
                  <option value="">--Select Operator--</option>
                  <option>Airtel</option>
                  <option>Vodafone</option>
                  <option>Jio</option>
                </select>
                        </div>
                   
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Enter Account No." />
                        </div>

                   
                         <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Enter Amount" />
                        </div>

                        <div class="col-md-2">
                            <button class="btn btn-primary btn-block">Recharge</button>
                        </div>
                    </div>
            </div>
        </div>
        <!-- Pills Navigation Style end -->
    </div>



     <br />
    <br />
     <br />
    <br />


</asp:Content>

