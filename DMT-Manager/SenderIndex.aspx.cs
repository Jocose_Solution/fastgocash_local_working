﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using InstantPayServiceLib;
using Newtonsoft.Json.Linq;

public partial class DMT_Manager_dmt_SenderIndex : System.Web.UI.Page
{
    private static string UserId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();
            Session["CurrIndexAgentId"] = Session["UID"].ToString();
        }
        else
        {
            Response.Redirect("/");
        }

        //List<string> result = InstantPay_ApiService.GetLatitudeLongnitude("NEW DELHI WEST DELHI DELHI");
    }


    #region [Jquery Section]
    [WebMethod]
    public static List<string> RemitterMobileSearch(string mobileno)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["CurrIndexAgentId"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                UserId = HttpContext.Current.Session["CurrIndexAgentId"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string response = InstantPay_ApiService.GetRemitterDetail(mobileno, UserId);

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            dynamic dyData = dyResult.data;
                            dynamic remitter = dyData.remitter;

                            string respoMobile = remitter.mobile;
                            string respoRemttId = remitter.id;
                            string consumedlimit = remitter.consumedlimit;
                            string remaininglimit = remitter.remaininglimit;

                            //HttpContext.Current.Session["RemtIdSearch"] = respoRemttId;

                            SqlTransactionDom stDom = new SqlTransactionDom();
                            int isUpdated = stDom.UpdateRemitterLimitConsumedRemaning(UserId, respoRemttId, respoMobile, consumedlimit, remaininglimit);

                            result.Add("success");
                            result.Add("/DMT-Manager/SenderDetails.aspx?mobile=" + respoMobile + "&sender=" + respoRemttId);
                        }
                        else if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                        {
                            //dynamic dyData = dyResult.data;
                            //dynamic remitter = dyData.remitter;

                            //string respoRemttId = remitter.id;

                            //result.Add("otpsent");
                            //result.Add(mobileno);
                            //result.Add(respoRemttId);
                            result.Add("registration");
                        }
                        else if (statusCode.ToLower() == "rnf" && statusMessage.ToLower() == "remitter not found")
                        {
                            result.Add("registration");
                        }
                        else if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "invalid mobile number")
                        {
                            result.Add("error");
                            result.Add(statusMessage);
                        }
                        else
                        {
                            result.Add("error");
                            result.Add(statusMessage);
                        }
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> RemitterRegistration(string mobile, string firstname, string lastname, string pincode, string localadd)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                string response = InstantPay_ApiService.RemitterRegistration(mobile, firstname, lastname, pincode, localadd, UserId);
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        string remitterid = remitter.id;

                        result.Add("success");
                        result.Add(remitterid);
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add(statusMessage);
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> RemitterVarification(string mobile, string remitterid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                string response = InstantPay_ApiService.RemitterRegistrationValidate(remitterid, mobile, otp, UserId);
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        //HttpContext.Current.Session["RemtIdSearch"] = remitterid;
                        result.Add("success");
                        result.Add("/dmt-manager/SenderDetails.aspx?mobile=" + mobile + "&sender=" + remitterid);
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add(statusMessage);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    //[WebMethod]
    //public static List<string> DirectRemitterMobileSearch(string mobileno)
    //{
    //    List<string> result = new List<string>();

    //    try
    //    {
    //        if (!string.IsNullOrEmpty(UserId))
    //        {
    //            string response = InstantPay_ApiService.GetRemitterDetail(mobileno, UserId);

    //            if (!string.IsNullOrEmpty(response))
    //            {
    //                dynamic dyResult = JObject.Parse(response);
    //                string statusCode = dyResult.statuscode;
    //                string statusMessage = dyResult.status;

    //                if (statusCode.Trim().ToLower() == "txn" && statusMessage.Trim().ToLower() == "transaction successful")
    //                {
    //                    dynamic dyData = dyResult.data;
    //                    dynamic remitter = dyData.remitter;

    //                    string respoMobile = remitter.mobile;
    //                    string respoRemttId = remitter.id;

    //                    result.Add("success");
    //                    result.Add("/dmt-manager/senderdetails.aspx?mobile=" + respoMobile + "&sender=" + respoRemttId);
    //                }
    //                else if (statusCode.Trim().ToLower() == "txn" && statusMessage.Trim().ToLower() == "otp sent successfully")
    //                {
    //                    dynamic dyData = dyResult.data;
    //                    dynamic remitter = dyData.remitter;

    //                    string respoMobile = remitter.mobile;
    //                    string respoRemttId = remitter.id;

    //                    result.Add("alreadyreg");
    //                    result.Add(respoRemttId);
    //                }
    //                //else if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
    //                //{
    //                //    dynamic dyData = dyResult.data;
    //                //    dynamic remitter = dyData.remitter;

    //                //    string respoRemttId = remitter.id;

    //                //    result.Add("otpsent");
    //                //    result.Add(mobileno);
    //                //    result.Add(respoRemttId);
    //                //}
    //                //else if (statusCode.ToLower() == "rnf" && statusMessage.ToLower() == "remitter not found")
    //                //{
    //                //    result.Add("registration");
    //                //}
    //                else if (statusCode.Trim().ToLower() == "err" && statusMessage.Trim().ToLower() == "invalid mobile number")
    //                {
    //                    result.Add("invalid");
    //                    result.Add(statusMessage);
    //                }
    //                else if (statusCode.Trim().ToLower() == "rnf" && statusMessage.Trim().ToLower() == "remitter not found")
    //                {
    //                    result.Add("notfound");
    //                    result.Add("remitter not registered");
    //                }
    //                else
    //                {
    //                    result.Add("error");
    //                    result.Add(statusMessage);
    //                }
    //            }
    //        }
    //        else
    //        {
    //            result.Add("reload");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        result.Add("error");
    //        result.Add(ex.Message);
    //    }

    //    return result;
    //}

    private static bool BindBankDetails()
    {
        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                return InstantPay_ApiService.BindBankDetails(UserId);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return false;
    }
    #endregion

    [WebMethod]
    public static List<string> ResendOtpToRemitterMobile(string mobileno)
    {
        List<string> result = new List<string>();

        try
        {
            try
            {
                if (!string.IsNullOrEmpty(UserId))
                {
                    string response = InstantPay_ApiService.GetRemitterDetail(mobileno, UserId);

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                        {                          
                            result.Add("otpsent");
                        }
                        else
                        {
                            result.Add("error");
                            result.Add(statusMessage);
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
                result.Add(ex.Message);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
}