﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="payout_direct_fund_transfer.aspx.cs" Inherits="DMT_Manager_payout_direct_fund_transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <link href="custom/css/validationcss.css" rel="stylesheet" />
    <link href="custom/css/datetime.css" rel="stylesheet" />

    <style>
        .boxSender {
            background: #ff414d;
            width: 100%;
            margin-left: 3px;
            color: #fff;
        }

        .p_botm {
            margin-bottom: 1px;
            padding: 4px;
        }

        .pmargin {
            margin-bottom: 4px;
        }

        .boxSenderdetail {
            border: 1px solid #ccc;
            width: 100%;
            margin-left: 3px;
        }

        .tab {
            overflow: hidden;
            border: 1px solid #ff414d;
            background-color: #f9f9f9;
        }

            /* Style the buttons inside the tab */
            .tab a {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 7px 32px;
                transition: 0.3s;
                font-size: 13px;
                text-align: center;
                color: #215821 !important
            }


                .tab a:hover {
                    background-color: #fff;
                    border: 1px solid #ccc;
                    border-bottom: none;
                    border-top: none;
                    color: #ff414d !important;
                }

                .tab a.active {
                    background-color: #fff;
                    border: 1px solid #ff414d;
                    border-bottom: none;
                    border-top: none;
                    color: #ff414d !important;
                }

        .tabcontent {
            display: none;
            padding: 6px 12px;
            /*border: 1px solid #ccc;*/
            border-top: none;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 1px solid #000000;
            background: #f1f5f6;
        }

        .table-sm th {
            padding: 3px;
            text-align: center;
            font-weight: 500;
        }

        .table-bordered th {
            border: 1px solid #000000;
        }

        .textboxamount {
            padding: 1px !important;
            /*width: 70% !important;*/
        }

        .table-sm td {
            padding: 10px;
            text-align: center;
            /*color: #828282;*/
        }

        .accountvalid {
            color: green;
            font-size: 22px;
        }

        .accountpending {
            color: #d47514;
            font-size: 22px;
        }

        .hidden {
            display: none;
        }

        .fltbtn {
            /*float: right;*/
            padding: 4px;
            margin-bottom: 4px;
            background: red;
            border: 1px solid red;
        }

        .scrolltable {
            overflow-y: auto;
            max-height: 600px;
        }

        .table thead th {
            position: sticky;
            top: 0;
            padding: 10px;
        }

        @media only screen and (min-width: 1200px) {
            .container {
                max-width: 1400px !important;
            }
        }

        .tooltip-inner {
            max-width: 350px !important;
        }
    </style>

    <div class="container">
        <div class="row">
            <div id="RemitterDetailsSection" class="col-sm-12" style="border: 1px solid #ccc; padding: 12px; background: #fff; margin-top: 12px; margin-bottom: 12px;">
                <%=RemitterDetail %>
            </div>

            <div class="col-sm-12" style="margin-top: 14px; background: #fff; margin-bottom: 14px; padding: 12px;">
                <div class="tab">
                    <a class="tablinks active" id="PayoutDirectTransfer" onclick="openTabSection(event, 'payoutdirect')"><span class="fa fa-hand-holding-usd fa-2x"></span>
                        <br />
                        <b>Payout Direct</b>
                    </a>
                    <a class="tablinks" onclick="openTabSection(event, 'payouthistory')" id="PayoutTransHistory"><span class="fa fa-history fa-2x"></span>
                        <br />
                        <b>Transaction History</b>
                    </a>
                </div>
                <style>
                    .help-error-block {
                        float: right !important;
                    }
                </style>
                <div id="payoutdirect" class="tabcontent bg-light shadow-md rounded p-4 mx-2" style="display: block; margin-top: 27px; border: 1px solid #ff414d59;">
                    <div class="row form-group">
                        <div class="col-sm-6 form-validation">
                            <label>Account Number</label>
                            <input id="txtAccountNumber" type="text" class="form-control" onkeypress="return isNumberValidationPrevent(event);" placeholder="Account Number" />
                        </div>
                        <div class="col-sm-6 form-validation">
                            <label>Re-Account Number</label>
                            <input id="txtReAccountNumber" type="text" class="form-control" onkeypress="return isNumberValidationPrevent(event);" placeholder="Re-Account Number" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6 form-validation">
                            <label>Bank</label>
                            <select id="ddlBindBankDrop" class="form-control">
                                <option value="">Select Bank</option>
                            </select>
                            <span id="ProcessBindingBank" class="hidden" style="position: absolute; right: 40px; color: #ff434f7d; top: 41px;">bank binding...<i class="fa fa-pulse fa-spinner"></i></span>
                        </div>
                        <div class="col-sm-6 form-validation">
                            <label>IFSC Code</label>
                            <input id="txtIFSCCode" type="text" class="form-control" placeholder="IFSC Code" />
                            <p id="typeofifsc" class="text-right" style="position: absolute; right: 25px; color: #ff434f7d; top: 45px;"></p>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6 form-validation">
                            <label>Transfer Mode</label>
                            <select id="ddlPayoutSp_Key" class="form-control">
                                <option value="">Select Mode</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-9 form-validation">
                                    <label>Beneficiary Name</label>
                                    <input id="txtBeneficiaryName" type="text" class="form-control" placeholder="Beneficiary Name" />
                                </div>
                                <div class="col-sm-3" style="margin-top: 12px;">
                                    <span id="btnGetBenName" style="cursor: pointer;" class="btn btn-primary btn-sm btn-block my-4" onclick="return GetBenNameById();">Get&nbsp;Name</span>
                                </div>
                                <p id="benerrormessage" class="text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>Alert Mobile Number</label>
                            <input id="txtAlertMobileNumber" type="text" class="form-control" placeholder="Alert Mobile Number (Example: 95xxxxxxxxx,95xxxxxxxxx)" />
                            <i class="fa fa-question-circle" aria-hidden="true" style="position: absolute; right: 0px; bottom: 20px; color: #ff414d;" data-placement="left" data-toggle="tooltip" title="In this 10 digit Mobile Number on which alert would be sent when beneficiary receives the funds.Multiple numbers can be sent for alert by using comma separated list. 95xxxxxxx,95xxxxxxx... etc."></i>
                        </div>
                        <div class="col-sm-6">
                            <label>Alert Email Id</label>
                            <input id="txtAlertEmailId" type="text" class="form-control" placeholder="Alert Email Id (Example: xxxxx@xxx.com, xxxxx@xxx.com)" />
                            <i class="fa fa-question-circle" aria-hidden="true" style="position: absolute; right: 0px; bottom: 20px; color: #ff414d;" data-placement="left" data-toggle="tooltip" title="Email ID on which alert would be sent when beneficiary receives the funds. Multiple emails can be sent for alert by using comma separated list. xxxxx@gmail.com,xxxxx@gmail.com etc."></i>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6 form-validation">
                            <label>Remark</label>
                            <textarea id="txtRemark" rows="4" class="form-control" placeholder="Remark"></textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Transfer Amount</label>
                            <input id="txtTransferAmount" type="text" class="form-control" placeholder="Transfer Amount" onkeypress="return isNumberValidationPrevent(event);" />
                            <span id="btnPayoutFundTransfer" style="cursor: pointer; float: right;" class="btn btn-primary btn-block my-4 col-sm-4" onclick="return ProcessToPayoutFundTransfer();">Fund Transfer</span>

                        </div>
                    </div>
                    <div class="form-group text-center">
                        <p id="payoutmessage" class="text-danger"></p>
                    </div>
                </div>

                <div id="payouthistory" class="tabcontent">
                    <div class="row">
                        <input type="button" value="Show Filter" class="btn btn-sm btn-primary" id="filterbtn" onclick="ShowHideDiv(this);" />
                    </div>
                    <div class="col-sm-12 form-group filterbox hidden" id="sidebar" style="margin: 10px;">
                        <div class="row form-group">
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">From Date</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control commonDate" placeholder="dd/mm/yyyy" id="txtTransFromDate" name="txtTransFromDate" style="padding: 4px;" />
                            </div>
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">To Date</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control commonDate" placeholder="dd/mm/yyyy" id="txtTransToDate" name="txtTransToDate" style="padding: 4px;" />
                            </div>
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">Track ID</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="txtTransTrackId" id="txtTransTrackId" class="form-control" placeholder="Track ID" style="padding: 4px;" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-2">Status</div>
                            <div class="col-sm-2">
                                <select id="ddlTransStatus" class="form-control" style="height: 36px!important; padding-top: 6px!important;">
                                    <option value="">All</option>
                                    <option value="successful">Success</option>
                                    <option value="under process">Under Process</option>
                                    <option value="failed">Failed</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <span class="btn btn-success btn-sm col-sm-7" style="padding: 4px; cursor: pointer;" id="btnTransFilter" onclick="PayoutTransFilter();">Search</span>
                                <span class="btn btn-danger btn-sm col-sm-3" style="padding: 4px; cursor: pointer;" id="btnClearTransFilter" onclick="ClearTransFilter();">Clear</span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row scrolltable">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Txn Date</th>
                                    <th>Order ID</th>
                                    <th>Track ID</th>
                                    <th>Mode</th>
                                    <th>Amount</th>
                                    <th>Chg</th>
                                    <th>Reciver</th>
                                    <th>Status</th>
                                    <th>Refund</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="PayoutFundTransRowDetails"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn btn-info btn-lg hidden updateaddresspopup" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#UpdateAddressPopup"></button>
    <div class="modal fade" id="UpdateAddressPopup" role="dialog">
        <div class="modal-dialog modal-md" style="margin: 5% auto!important;">
            <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
                <div class="modal-body" style="padding-left: 20px!important;">
                    <div class="container">
                        <div class="login-signup-page mx-auto">
                            <h4 class="font-weight-400 text-center">Update Current / Local Address
                                <button type="button" id="UpdateAddressClose" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </h4>
                            <div class="bg-light shadow-md rounded p-4 mx-2">
                                <div class="form-group form-validation">
                                    <textarea id="txtUpdateCurrLocalAddress" class="form-control" placeholder="Current / Local Address"></textarea>
                                </div>
                                <span id="btnUpdateCurrLocalAddress" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="return UpdateCurrLocalAddress();">Update Address</span>
                                <p id="updatemsg"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn btn-info btn-lg hidden fundtransotp" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#SendFundTransOtp"></button>
    <div class="modal fade" id="SendFundTransOtp" role="dialog">
        <div class="modal-dialog modal-md" style="margin: 5% auto!important;">
            <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
                <div class="modal-body" style="padding-left: 20px!important;">
                    <div class="container">
                        <div class="login-signup-page mx-auto">
                            <h4 class="font-weight-400 text-center"><span id="OTPSentHeading">Fund Transfer : OTP Verification</span>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="otpsucsclose">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </h4>
                            <div class="bg-light shadow-md rounded p-4 mx-2" id="OTPSendContent">
                                <p style="color: #ff414d;" id="otpsentmessage"></p>

                                <input id="txtFundTransOtp" type="text" class="form-control" placeholder="OTP Code" />

                                <span id="btnFundTransOtpBtn" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="return ProcessToVerifyFundOtp();">Verify OTP</span>
                                <p id="fundtransotpmsg"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<%--    <button class="show-example-btn hidden" aria-label="Show SweetAlert2 success message" id="SwalSuccessMsg" onclick="return executeExample()"></button>--%>

    <script src="custom/js/payoutdirect.js"></script>
    <script src="custom/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        function openTabSection(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        function ShowHideDiv(btnPassport) {
            if (btnPassport.value == "Show Filter") {
                $(".filterbox").show(50).removeClass("hidden");
                btnPassport.value = "Hide Filter";
            } else {
                btnPassport.value = "Hide Filter";
                $(".filterbox").hide(50).addClass("hidden");
                btnPassport.value = "Show Filter";
            }
        }

        $('[data-toggle="tooltip"]').tooltip();

        $('.commonDate').datepicker({
            dateFormat: "dd/mm/yy",
            showStatus: true,
            showWeeks: true,
            currentText: 'Now',
            autoSize: true,
            maxDate: -0,
            gotoCurrent: true,
            showAnim: 'blind',
            highlightWeek: true
        });
    </script>
</asp:Content>

