﻿
Imports System.Data

Partial Class MasterPagefor_money_transfer
    Inherits System.Web.UI.MasterPage
    Public AgencyName As String = ""
    Public AgencyId As String = ""
    Private det As New Details()
    Private ds As DataSet
    Public Agent_status As String = ""
    Public Agent_Payout_Status As String = ""
    Public Is_Agent_Payout_Status As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Session("UID") <> "" AndAlso Session("UID") IsNot Nothing Then
                AgencyName = Session("AgencyName")
                AgencyId = Session("AgencyId")
                ds = det.AgencyInfo(Session("UID").ToString())
                If ds.Tables(0).Rows.Count > 0 Then
                    Agent_status = ds.Tables(0).Rows(0)("Agent_status").ToString()
                    Agent_Payout_Status = ds.Tables(0).Rows(0)("Agent_Payout_Status").ToString()
                    If Agent_status = "NOT ACTIVE" Then
                        Logout_Click(sender, e)
                    End If
                    If Agent_Payout_Status.Trim() = "ACTIVE" Then
                        Is_Agent_Payout_Status = True
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Logout_Click(sender As Object, e As EventArgs)
        Try
            FormsAuthentication.SignOut()
            Session.Abandon()
            Response.Redirect("~/Login.aspx")
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
End Class

