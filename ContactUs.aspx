﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        /* General styles */

        body {
            background-color: #f3f3f3;
            font-family: Futura, sans-serif;
        }

        .wrapper {
            margin: 24px 180px;
        }

        h1 {
            color: #716eb6;
            text-align: center;
        }

        /* Table styles */

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        td,
        th {
            padding: 0;
            text-align: left;
        }

            td:first-of-type {
                padding-left: 36px;
                width: 66px;
            }

        .c-table {
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            background-color: #fff;
            border-radius: 4px;
            font-size: 12px;
            line-height: 1.25;
            margin-bottom: 24px;
            width: 100%;
        }

        .c-table__cell {
            padding: 12px 6px 12px 12px;
            word-wrap: break-word;
        }

        .c-table__header tr {
            color: #fff;
        }

        .c-table__header th {
            background-color: #716eb6;
            padding: 18px 6px 18px 12px;
        }

            .c-table__header th:first-child {
                border-top-left-radius: 4px;
            }

            .c-table__header th:last-child {
                border-top-right-radius: 4px;
            }

        .fontW {
            font-size: 18px;
            color: #2f70a9;
        }
        .fontfamily_contact {
    font-size: 24px;
    text-decoration: none;
    font-family: sans-serif;
}
        .c-table__body tr {
            border-bottom: 1px solid rgba(113, 110, 182, 0.15);
        }

            .c-table__body tr:last-child {
                border-bottom: none;
            }

            .c-table__body tr:hover {
                background-color: rgba(113, 110, 182, 0.15);
                color: #272b37;
            }

        .c-table__label {
            display: none;
        }

        /* Mobile table styles */

        @media only screen and (max-width: 767px) {

            table, thead, tbody, th, td, tr {
                display: block;
            }

                td:first-child {
                    padding-top: 24px;
                }

                td:last-child {
                    padding-bottom: 24px;
                }

            .c-table {
                border: 1px solid rgba(113, 110, 182, 0.15);
                font-size: 15px;
                line-break: 1.2;
            }

            .c-table__header tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            .c-table__cell {
                padding: 12px 24px;
                position: relative;
                width: 100%;
                word-wrap: break-word;
            }

            .c-table__label {
                color: #272b37;
                display: block;
                font-size: 10px;
                font-weight: 700;
                line-height: 1.2;
                margin-bottom: 6px;
                text-transform: uppercase;
            }

            .c-table__body tr:hover {
                background-color: transparent;
            }

            .c-table__body tr:nth-child(odd) {
                background-color: rgba(113, 110, 182, 0.04);
            }
        }
    </style>
    <section>
        <div class="form form-spac rows con-page">
            <div class="container">


                <div class="wrapper">
                    <h1>Contact Us</h1>
                    <div class="row">
                        <div class="col-sm-6" style="border: 1px solid #ccc;border-right: 0px;">
                         
                                <b class="fontW">Got a question? Okay Find The Solution !</b>
                                <hr>
                                <div>
                                    <span class="fa fa-bullhorn" style="font-size: 28px; color: #337ab7;"></span>&nbsp;&nbsp;
                                <a class="fontfamily_contact" href="tel:+91-9760443317">24x7 Support</a>
                                </div>

                                <hr>
                                <div>
                                    <span class="fa fa-phone" style="font-size: 28px; color: #337ab7;"></span>&nbsp;&nbsp;
                                    <a class="fontfamily_contact" href="tel:+91-9760443317">+91-9760443317</a>
                                </div>
                                <hr>
                                <div>
                                    <span class="fa fa-envelope " style="font-size: 28px; color: #337ab7;"></span>&nbsp;&nbsp;
                                 <a class="fontfamily_contact" href="mailto:fastgocash@gmail.com">fastgocash@gmail.com</a>
                                </div>
                                <hr>
                            
                        </div>
                    
                    <div class="col-sm-6" style="border: 1px solid #ccc;">
                         <b class="fontW">Address</b>
                        <p style="color: #526681;">Shop no 4 Zila Parishat Gate Opposite A D Basic Chaupla Road Bareilly</p>
                    </div>
                    </div>
                    <h1 style="font-size: 14px;">Not satisfied with the Services Mobile no 7409455555 and email info@fastgocash.com</h1>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

